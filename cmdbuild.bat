@echo off
REM on your system, in cmd.exe type: date /t. if you have - or / in that string, set delim to that char
SET delim=/
SET output=%cd%
FOR /F "tokens=1-5 delims=%delim% " %%d IN ("%date%") DO SET builddate=%%d%%e%%f
FOR /F "tokens=1-5 delims=: " %%t IN ("%time%") DO SET buildtime=%%t%%u
FOR /F "delims=" %%a in ('cd') DO SET buildfolder=%%~nxa
SET filename=_%buildfolder%_%builddate%_%buildtime%.zip
echo %builddate% %buildfolder% %buildtime%
ECHO creating zip named: %filename%
REM somebody here is returning an exit code which is messing up the batch file, so it's all strung together
dart pub global run webdev build && cd build && 7z a -tzip "%filename%" slide.dart.js *.css slide.html assets/ && ECHO moving %filename% to "%output%\" && move "%output%\build\%filename%" "%output%"
PAUSE