//import 'package:clickspacetv/private.dart';
import 'package:clickspacetv/api_http.dart';
import 'package:clickspacetv/slide.dart';

import 'display/client_menu_field.dart';
import 'display/main_display.dart';
import 'logic/bv_monitor_generator.dart';
import 'logic/client_settings.dart';

void main() {
  new _TemplateMenuV1();
}

class _TemplateMenuV1 extends CstvSlide {
  //Monitor
  BvMonitor _monitor;
  //Display Elements
  MainDisplay mainDisplay;

  //=========================================
  // CONSTRUCTOR / INIT
  //===============================
  _TemplateMenuV1() {
    setOrientation(CstvSlide.ORIENTATION_HORIZONTAL);
  }

  //=========================================
  // OVERRIDES
  //===============================
  void debugSetup() {
    int totalSkins = ClientSettings.clients.length;

    AppData.isInFrameMode = false;
    Data.slideSkin = totalSkins;
    //(new DateTime.now().second % totalSkins) + 1;
    Data.text1 = '1';
    Data.text2 = '40';
    Data.text3 = '2'; //(new Random().nextInt(totalTvs) + 1).toString();
    Data.text4 = '2';
    Data.slideDuration = 3600;

    print(
        'Running slideSkin ${Data.slideSkin} with minStock ${Data.text1}, swapSpeed ${Data.text2}, TV# ${Data.text3} of ${Data.text4}');
    //AppData.orientation = CstvSlide.ORIENTATION_HORIZONTAL;
    AppData.orientation = CstvSlide.ORIENTATION_VERTICAL;
  }

  bool onInitLoopIsSlideDataComplete() {
    if (Data.slideSkin == null ||
        Data.slideSkin < 1 ||
        Data.slideSkin > ClientSettings.clients.length) return false;

    int tvNumber = int.tryParse(Data.text3.toString());
    int totalTvs = int.tryParse(Data.text4.toString()) ?? 3;
    if (tvNumber == null || tvNumber > totalTvs) return false;

    if (_monitor == null) {
      //Fallback self-destruct
      launchSelfDestructTimer();

      //parse data - i.e. swap times, stock levels, etc
      int minStock = int.tryParse(Data.text1.toString());
      int swapSpeed = int.tryParse(Data.text2.toString());

      if (swapSpeed != null && swapSpeed > 2 && swapSpeed < 500) {
        ClientMenuField.scrollSpeedPxPerSec = swapSpeed;
        ClientMenuField.pagedTransitionSpeedSec = swapSpeed;
      }

      // Set up the tvSpecificCategoryBlocklist. Case insensitve, and partial
      // matches will be blocked. For example, if TV#1 doesn't need info about
      // the "flower" category, add "flower" here and this TV won't ever
      // download those products. Only blocks on POS categpries, not format
      // or strain type, so consult the console log on first run to get the
      // list of categories that are being listened to.
      List<String> tvSpecificCategoryBlocklist = [];

      //setup the monitor
      _monitor = BvMonitorGenerator.generate(
          minStock,
          tvSpecificCategoryBlocklist,
          ClientSettings.clients[Data.slideSkin].credentials);
    }
    if (_monitor.isInitialized == false) {
      return false;
    }

    //monitor may not be ready, but it's at least initialized. Continue with display logic.
    return true;
  }

  void selfDestructTimerHandler() {
    if (_monitor == null || _monitor.isReady == false) {
      CommunicationManager.sendMessageSelfDestruct('_monitor not ready');
    } else if (mainDisplay == null) {
      CommunicationManager.sendMessageSelfDestruct('cstvDisplay not ready');
    }
  }

  bool onInitLoopIsSlideReady() {
    if (AppData.isInFrameMode == true) {
      throw new ArgumentError(
          'Error: AppData.isInFrameMode == true, but this slide does not support FrameMode.');
    } else {
      setOrientation(AppData.orientation);
      int tvNumber = int.parse(Data.text3.toString());
      mainDisplay = new MainDisplay(display, _monitor, tvNumber);
    }

    return true;
  }

  bool doesSlideWantToPlay() {
    return true;
  }

  void onPlaySlide() {
    mainDisplay.play();
  }

  void onStopSlide() {
    mainDisplay.stop();
  }

  void onKillSlide() {}
}
