//=========================================
// IMPORTS
//===============================
import 'dart:html';
import 'package:clickspacetv/slide.dart';
import 'package:clickspacetv/api_http.dart';
import '../logic/client_settings.dart';
import 'fonts.dart';
import 'client_menu_field.dart';

//=========================================
// CLASS WRAPPER
//===============================
class MainDisplay extends CstvDisplay {
  //=========================================
  // VARS
  //===============================
  final BvMonitor monitor;
  final int tvNumber;
  ClientMenuField _menuField;
  bool isHorizontal = AppData.orientation == AppData.ORIENTATION_HORIZONTAL;
  ClientSettings _clientSettings = ClientSettings.clients[Data.slideSkin];

  //=========================================
  // CONSTRUCTOR / INIT
  //===============================
  MainDisplay(DivElement display, this.monitor, this.tvNumber)
      : super(display) {
    //Allow the slide to run infinitely, instead of ending after Data.slideDuration is done.
    CommunicationManager.sendMessageAllowRunInfinite(true);

    //Register the Fonts:
    Fonts.registerFonts();
    //Get client data
    String theme = _clientSettings.useLightTheme == false ? "dark" : "light";

    //Get the correct dimensions for the logo
    int height = isHorizontal ? 1080 : 1920;
    int width = isHorizontal ? 1920 : 1080;

    //Setup the display elements - underlays, the menuField, etc
    TextField companyName = new TextField(
        text: _clientSettings.clientName,
        fontSizeMin: 40,
        y: isHorizontal ? 30 : 50,
        width: width,
        color: _clientSettings.useLightTheme ? "#191919" : '#efefef',
        fontSizeMax: 40,
        font: Fonts.gothamUltra,
        textAlign: TextField.CENTER);
    ImageField bg = new ImageField()
      ..src = "assets/img/$theme${isHorizontal ? 'H' : 'V'}.png"
      ..setPosition(0, 0)
      ..setDimensions(isHorizontal ? 1920 : 1080, isHorizontal ? 1080 : 1920);
    ImageField logo = new ImageField()
      ..src = "assets/logo/${_clientSettings.clientLogo}"
      ..opacity = "0.25"
      ..setPosition(width / 2 - (_clientSettings.logoWidth / 2),
          height / 2 - (_clientSettings.logoHeight / 2))
      ..setDimensions(_clientSettings.logoWidth, _clientSettings.logoHeight);

    //Setup the display elements - underlays, the menuField, etc
    _menuField = new ClientMenuField(monitor, tvNumber, 3, isHorizontal);
    //Append elements to the display.
    display.append(bg.element);
    display.append(logo.element);
    display.append(companyName.element);
    display.append(_menuField.element);

    //If keying animation to the background video, add Keyframes via keyframeManager.createKeyframe()

    //Set a few convenience css classes
    CssManager.setStyleForClass('smallFont', 'font-size:70%');

    //To position one, where we set static text:
    toPositionOne();
  }

  void play() {
    _menuField.play();
  }

  void stop() {
    _menuField.stop();
    toPositionOne();
  }

  //=========================================
  // ANIMATION FUNCTIONS
  //===============================
  void toPositionOne() {
    Animator.killAllAnimations();
    _menuField.toPositionOne();
  }
}
