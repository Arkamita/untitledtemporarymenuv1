import '../logic/client_settings.dart';
import 'client_helper.dart';
import 'fonts.dart';
import 'package:clickspacetv/slide.dart';
import 'package:clickspacetv/api.dart';

class ClientLineItem extends CanvasField {
  BvItem _product;
  bool _isHorizontal;
  String fontColor;
  ClientSettings _clientSettings = ClientSettings.clients[Data.slideSkin];

  ClientLineItem.fromPlan(ClientLineItemPlan _plan) {
    setDimensions(_plan.width, _plan.height);
    setPosition(_plan.x, _plan.y);
    _product = _plan._product;
    _isHorizontal = _plan.isHorizontal;
    fontColor = _clientSettings.useLightTheme ? "#191919" : '#efefef';
    if (_product == null || _product.isEmpty) {
      //just a spacer
      return;
    }

    _buildIcon();
    _buildStrainTypeTf();

    if (_product.isOnSale) {
      _buildFlag('SALE');
    }

    _buildNameTf();
    _buildPriceSizeTf();
    _buildThcTf();
    _buildCbdTf();
  }

  void _buildIcon() {
    if (!BvItem.FORMATS.contains(_product.format)) {
      return;
    }
    this.drawImage(
      src: "assets/img/format/${_product.format.toLowerCase()}.png",
      x: _isHorizontal ? 35 : 24,
      y: -1,
      width: 89,
      height: 90,
      filter: ClientHelper.getFilterByStrainType(_product),
    );
  }

  void _buildStrainTypeTf() {
    this.drawText(
      text: _product.strainType,
      x: _isHorizontal ? 37 : 25,
      y: 68,
      maxWidth: 85,
      fontFamily: Fonts.gothamBook,
      fontSize: 14,
      fillColor: fontColor,
      textAlign: TextField.CENTER,
      letterSpacing: "0.050em",
      toUpperCase: true,
    );
  }

  void _buildFlag(String text) {
    this.drawText(
      text: text,
      x: _isHorizontal ? 141 : 129,
      y: 9,
      maxWidth: 100,
      fontFamily: Fonts.gothamUltra,
      fontSize: 16,
      fillColor: "#e27426",
      letterSpacing: "0.060em",
    );
  }

  void _buildNameTf() {
    this.drawText(
      text: _product.productName,
      x: _isHorizontal ? 136 : 124,
      y: 21,
      maxWidth: _isHorizontal ? 1225 : 563,
      fontFamily: Fonts.gothamMedium,
      fontSize: 36,
      fillColor: fontColor,
      letterSpacing: "0.060em",
      toUpperCase: true,
      addEllipses: true,
      minHorizontalScale: 2 / 3,
    );
  }

  void _buildPriceSizeTf() {
    List<TextBlock> textBlocks = [];
    for (BvItem variant in _product.variantsOrThis) {
      if (variant.inStock) {
        if (!textBlocks.isEmpty) {
          textBlocks.add(TextBlock(
            text: " | ",
            fontSize: 19,
            y: 62,
          ));
        }
        textBlocks.add(TextBlock(
          text: "${variant.currentPrice} ",
          fontSize: 24,
          y: 57,
        ));
        textBlocks.add(TextBlock(
          text: variant.netSizeSummary,
          fontSize: 19,
          y: 62,
        ));
      }
    }

    this.drawMultiStyledText(
      textBlocks: textBlocks,
      x: _isHorizontal ? 135 : 123,
      maxWidth: _isHorizontal ? 1365 : 563,
      fontFamily: Fonts.gothamMedium,
      fillColor: fontColor,
      letterSpacing: "0.060em",
    );
  }

  void _buildThcTf() {
    this.drawText(
      text: _product.getThcRangeStyled(
          addUom: true, preferredUom: BvItem.UOM_PERCENT),
      x: _isHorizontal ? 1633 : 768,
      y: 27,
      maxWidth: _isHorizontal ? 180 : 98,
      fontFamily: Fonts.gothamMedium,
      fontSize: 21,
      textAlign: TextField.CENTER,
      fillColor: fontColor,
      letterSpacing: "0.060em",
    );
  }

  void _buildCbdTf() {
    this.drawText(
      text: _product.getCbdRangeStyled(
          addUom: true, preferredUom: BvItem.UOM_PERCENT),
      x: _isHorizontal ? 1436 : 874,
      y: 27,
      maxWidth: _isHorizontal ? 180 : 98,
      fontFamily: Fonts.gothamMedium,
      fontSize: 21,
      textAlign: TextField.CENTER,
      fillColor: fontColor,
      letterSpacing: "0.060em",
    );
  }
}

//=========================================
// PLAN CLASS
//===============================
class ClientLineItemPlan extends ExtendableFieldPlan {
  static const num HEIGHT = 112;

  final BvItem _product;
  final bool isHorizontal;
  final num tvNumber;

  ClientLineItemPlan(this._product, this.isHorizontal, this.tvNumber) {
    setPosition(0, 0);
    setDimensions(isHorizontal ? 1920 : 1080, HEIGHT);
  }

  ClientLineItem build() {
    ClientLineItem item = new ClientLineItem.fromPlan(this);
    place(item);
    buildChildren(item);
    return item;
  }
}
