import 'package:clickspacetv/slide.dart' show FontManager;

class Fonts{

  static const String gothamUltra = "gothamUltra";
  static const String gothamMedium = "gothamMedium";
  static const String gothamBook = "gothamBook";


  static void registerFonts(){
    FontManager.registerFont(gothamUltra, "assets/font/gothamUltra.otf");
    FontManager.registerFont(gothamMedium, "assets/font/gothamMedium.otf");
    FontManager.registerFont(gothamBook, "assets/font/gothamBook.otf");



  }

}