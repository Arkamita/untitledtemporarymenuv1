import '../logic/client_settings.dart';
import 'fonts.dart';
import 'package:clickspacetv/slide.dart';

class ClientHeader extends CanvasField {
  bool _isHorizontal;
  String fontColor;
  ClientSettings _clientSettings = ClientSettings.clients[Data.slideSkin];

  ClientHeader.fromPlan(ClientHeaderPlan _plan) {
    setDimensions(_plan.width, _plan.height);
    setPosition(_plan.x, _plan.y);
    _isHorizontal = _plan.isHorizontal;
    fontColor = _clientSettings.useLightTheme ? "#191919" : '#efefef';

    _buildCategoryTf(_plan.format);
    _buildThcTf();
    _buildCbdTf();
  }

  void _buildCategoryTf(String format) {
    this.drawText(
      text: format,
      x: _isHorizontal ? 140 : 128,
      y: 61,
      maxWidth: 500,
      fontFamily: Fonts.gothamUltra,
      fontSize: 24,
      fillColor: fontColor,
      letterSpacing: "0.060em",
      toUpperCase: true,
    );
  }

  void _buildThcTf() {
    this.drawText(
      text: "THC",
      x: _isHorizontal ? 1703 : 794,
      y: 66,
      maxWidth: 150,
      fontFamily: Fonts.gothamUltra,
      fontSize: 18,
      fillColor: fontColor,
      letterSpacing: "0.060em",
    );
  }

  void _buildCbdTf() {
    this.drawText(
      text: "CBD",
      x: _isHorizontal ? 1513 : 900,
      y: 66,
      maxWidth: 150,
      fontFamily: Fonts.gothamUltra,
      fontSize: 18,
      fillColor: fontColor,
      letterSpacing: "0.060em",
    );
  }
}

//=========================================
// PLAN CLASS
//===============================
class ClientHeaderPlan extends ExtendableFieldPlan {
  static const num HEIGHT = 112;

  final String format;
  final bool isHorizontal;
  final num tvNumber;

  ClientHeaderPlan(this.format, this.isHorizontal, this.tvNumber) {
    setPosition(0, 0);
    setDimensions(isHorizontal ? 1920 : 1080, HEIGHT);
  }

  ClientHeader build() {
    ClientHeader item = new ClientHeader.fromPlan(this);
    place(item);
    buildChildren(item);
    return item;
  }
}
