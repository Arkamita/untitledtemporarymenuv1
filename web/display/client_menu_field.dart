import 'dart:math';
import 'package:clickspacetv/slide.dart';
import 'package:clickspacetv/api.dart';
import 'package:clickspacetv/api_http.dart';
import '../logic/client_settings.dart';
import 'client_header.dart';
import 'client_line_item.dart';

//=========================================
// CLASS WRAPPER
//===============================
class ClientMenuField extends BvMenuField {
  static int scrollSpeedPxPerSec = 40;
  static int pagedTransitionSpeedSec = 15;
  final bool isHorizontal;

  //=========================================
  // CONSTRUCTOR
  //===============================
  ClientMenuField(
      BvMonitor monitor, int tvNumber, int totalTvs, this.isHorizontal)
      : super(
          monitor: monitor,
          mode: ClientSettings.clients[Data.slideSkin].mode,
          tvNumber: tvNumber,
          totalTvs: totalTvs,
          singleScreenAllowed: false,
          columnCount: 1,
          stickyHeaders:
              true, //if isPagedMode == false, you can have headers move with the line items, instead of being static
          fillTopToBottom:
              true, //true: fills a column entirely before moving onto the next. false: fills the columns left to right, then cycles back
          orientation: isHorizontal
              ? CstvSlide.ORIENTATION_HORIZONTAL
              : CstvSlide.ORIENTATION_VERTICAL,
          swapInterval: new Duration(seconds: pagedTransitionSpeedSec),
        ) {}

  //=========================================
  // GETTERS
  //===============================
  int get maxItemsPerColumn => isHorizontal ? 8 : 15;

  //=========================================
  // REQUIRED IMPLEMENTATIONS
  //===============================
  List<BvItem> generateAllTvProductList() {
    //unusally unused. Only has unique logic if all products want to fit on
    //one page, and singleScreenAllowed == true
    return generateSingleTvProductList();
  }

  List<BvItem> generateSingleTvProductList() {
    return ClientSettings.clients[Data.slideSkin]
        .getProductList(monitor, tvNumber);
  }

  BvColumnFieldPlan createEmptyColumnPlan(
      int columnIndex, bool isScrolling, int reduceColumnHeightMultiplier) {
    BvColumnFieldPlan columnPlan = new BvColumnFieldPlan(
        columnIndex, isScrolling,
        scrollSpeedPxPerSecond: scrollSpeedPxPerSec);
    //If the column doesn't fill the available area, how should it be centred?
    //0.0 topAlign, 1.0 bottomAlign, 0.5 middleAlign. 0.0 is typical.
    num vAlignMultiplier = 0.0;

    //Set x and y
    columnPlan.x = 0 + (columnIndex * 0);
    columnPlan.y = isHorizontal
        ? 41
        : 112; //where the column should start, before vAlign adjustments

    //Set width. Height is calculated based on externals.
    columnPlan.width = isHorizontal ? 1920 : 1080;

    //The column height is 1px (for rounding errors),
    // plus the ClientHeaderPlan height (if applicable),
    // plus ClientLineItemPlan.HEIGHT * items allowed in this column
    columnPlan.height = 1;
    columnPlan.height += ClientHeaderPlan.HEIGHT;
    columnPlan.height += ClientLineItemPlan.HEIGHT *
        (maxItemsPerColumn - max(0, reduceColumnHeightMultiplier));

    //Move the y value up if reduceColumnHeightMultiplier > 0:
    columnPlan.y += (ClientLineItemPlan.HEIGHT *
        max(0, reduceColumnHeightMultiplier) *
        vAlignMultiplier);
    return columnPlan;
  }

  ExtendableFieldPlan createHeaderPlanIfRequired(
      int columnIndex,
      bool isScrolling,
      BvItem lastProductInColumn,
      BvItem nextProductInColumn) {
    if (nextProductInColumn == null || nextProductInColumn.isEmpty) {
      return null;
    }
    if (nextProductInColumn.format != lastProductInColumn?.format) {
      return new ClientHeaderPlan(
          nextProductInColumn.format, isHorizontal, tvNumber);
    }
    return null;
  }

  ExtendableFieldPlan createInlineHeaderPlanIfRequired(
      int columnIndex,
      bool isScrolling,
      BvItem lastProductInColumn,
      BvItem nextProductInColumn) {
    return null;
  }

  ExtendableFieldPlan createLineItemPlan(BvItem forProduct, int columnIndex) {
    return new ClientLineItemPlan(forProduct, isHorizontal, tvNumber);
  }

  //=========================================
  // OPTIONAL IMPLEMENTATIONS
  //===============================
  //List<DivFieldPlan> preBuildHook(List<DivFieldPlan> solution) => solution;
  //DivFieldPlan createEmptyPagePlan(bool useInfiniteScroll){ }
  //bool isRefreshRequired(DateTime lastCheck){}

  //=========================================
  // OPTIONAL IMPLEMENTATIONS - PAGED ONLY
  //===============================
  //bool forceNewPage(int columnIndex, BvItem lastProductInColumn, BvItem lastProductOnPage, BvItem nextProductInColumn) => false;
  //void fadeInPageHook(int pageIndex, {int delay=null}){}
  //void fadeOutPageHook(int pageIndex){}

}
