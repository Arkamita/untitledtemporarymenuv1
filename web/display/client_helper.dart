import 'package:clickspacetv/api.dart';
import 'package:clickspacetv/api_http.dart';

class ClientHelper {
  static List<BvItem> getOutAndAboutProductList(
      BvMonitor monitor, int tvNumber) {
    // TV1 - Flower (vertical) & Pre roll
    // TV2 - Vapes + Concentrates (vertical)
    // TV3 - Beverages + Edibles + Oils + Caps (Vertical)
    List<BvItem> toReturn = [];
    if (tvNumber == 1) {
      toReturn.addAll(monitor.flowerProductsInStock);
      toReturn.addAll(monitor.prerollProductsInStock);
    } else if (tvNumber == 2) {
      toReturn.addAll(monitor.vapeProductsInStock);
      toReturn.addAll(monitor.concentrateProductsInStock);
    } else if (tvNumber == 3) {
      toReturn.addAll(monitor.beverageProductsInStock);
      toReturn.addAll(monitor.edibleProductsInStock);
      toReturn.addAll(monitor.oilProductsInStock);
      toReturn.addAll(monitor.capsuleProductsInStock);
    }

    return toReturn;
  }

  static List<BvItem> getTakeMeHomeProductList(
      BvMonitor monitor, int tvNumber) {
    // TV1 - Flower (horizontal)
    // TV2 - Vapes + Prerolls (horizontal)
    // TV3 - Edibles, Beverages, Concentrates (Horizontal)
    List<BvItem> toReturn = [];
    if (tvNumber == 1) {
      toReturn.addAll(monitor.flowerProductsInStock);
    } else if (tvNumber == 2) {
      toReturn.addAll(monitor.vapeProductsInStock);
      toReturn.addAll(monitor.prerollProductsInStock);
    } else if (tvNumber == 3) {
      toReturn.addAll(monitor.edibleProductsInStock);
      toReturn.addAll(monitor.beverageProductsInStock);
      toReturn.addAll(monitor.concentrateProductsInStock);
    }
    return toReturn;
  }

  static List<BvItem> getHoneyPotProductList(BvMonitor monitor, int tvNumber) {
    //TV#1 - Hybrid Flower + Prerolls - Horizontal
    //TV#2 - Sativa Flower + Prerolls - Horizontal
    //TV#3 -Indica Flower + Prerolls - Horizontal
    List<BvItem> toReturn = [];
    if (tvNumber == 1) {
      toReturn
          .addAll(monitor.flowerProductsInStock.where((item) => item.isHybrid));
      toReturn.addAll(
          monitor.prerollProductsInStock.where((item) => item.isHybrid));
    } else if (tvNumber == 2) {
      toReturn
          .addAll(monitor.flowerProductsInStock.where((item) => item.isSativa));
      toReturn.addAll(
          monitor.prerollProductsInStock.where((item) => item.isSativa));
    } else if (tvNumber == 3) {
      toReturn
          .addAll(monitor.flowerProductsInStock.where((item) => item.isIndica));
      toReturn.addAll(
          monitor.prerollProductsInStock.where((item) => item.isIndica));
    }
    return toReturn;
  }

  static List<BvItem> getRokebyProductList(BvMonitor monitor, int tvNumber) {
    // TV1 - Flower + Preroll (vertical)
    // TV2 - Vapes + Edibles + Beverages(vertical)
    // TV3 - Topcials, Concentrates, oils, caps (vertical)
    List<BvItem> toReturn = [];
    if (tvNumber == 1) {
      toReturn.addAll(monitor.flowerProductsInStock);
      toReturn.addAll(monitor.prerollProductsInStock);
    } else if (tvNumber == 2) {
      toReturn.addAll(monitor.vapeProductsInStock);
      toReturn.addAll(monitor.edibleProductsInStock);
      toReturn.addAll(monitor.beverageProductsInStock);
    } else if (tvNumber == 3) {
      toReturn.addAll(monitor.topicalProductsInStock);
      toReturn.addAll(monitor.oilProductsInStock);
      toReturn.addAll(monitor.concentrateProductsInStock);
      toReturn.addAll(monitor.capsuleProductsInStock);
    }
    return toReturn;
  }

  static List<BvItem> getWCCProductList(BvMonitor monitor, int tvNumber) {
    // TV1 - Flower (vertical) & Pre roll
    // TV2 - Vapes + Concentrates (vertical)
    // TV3 - Beverages + Edibles + Oils + Caps (Vertical)
    List<BvItem> toReturn = [];
    if (tvNumber == 1) {
      toReturn.addAll(monitor.flowerProductsInStock);
    } else if (tvNumber == 2) {
      toReturn.addAll(monitor.prerollProductsInStock);
    } else if (tvNumber == 3) {
      toReturn.addAll(monitor.vapeProductsInStock);
    } else if (tvNumber == 4) {
      toReturn.addAll(monitor.beverageProductsInStock);
      toReturn.addAll(monitor.edibleProductsInStock);
      toReturn.addAll(monitor.oilProductsInStock);
      toReturn.addAll(monitor.capsuleProductsInStock);
      toReturn.addAll(monitor.concentrateProductsInStock);
    }

    return toReturn;
  }

  static List<BvItem> getHighlandProductList(BvMonitor monitor, int tvNumber) {
    // TV1 - Flower (vertical) & Pre roll
    // TV2 - Vapes + Concentrates (vertical)
    // TV3 - Beverages + Edibles + Oils + Caps (Vertical)
    List<BvItem> toReturn = [];
    if (tvNumber == 1) {
      toReturn.addAll(monitor.flowerProductsInStock);
      toReturn.sort(highlandSort);
    } else if (tvNumber == 2) {
      toReturn.addAll(monitor.flowerProductsInStock);
      toReturn.sort(highlandSort);
    } else if (tvNumber == 3) {
      toReturn.addAll(monitor.prerollProductsInStock);
      toReturn.sort(highlandSort);
    } else if (tvNumber == 4) {
      toReturn.addAll(monitor.vapeProductsInStock);
      toReturn.sort(highlandSort);
    } else if (tvNumber == 5) {
      toReturn.addAll(monitor.beverageProductsInStock);
      toReturn.addAll(monitor.edibleProductsInStock);
      toReturn.sort(BvMonitorAbstract.sortOnLowestPriceFirst);
    } else if (tvNumber == 6) {
      toReturn.addAll(monitor.concentrateProductsInStock);
    } else if (tvNumber == 7) {
      toReturn.addAll(monitor.oilProductsInStock);
      toReturn.addAll(monitor.topicalProductsInStock);
    }

    return toReturn;
  }

  static int highlandSort(BvItem a, BvItem b) {
    if ((a.thcAvg ?? -1) != (b.thcAvg ?? -1)) {
      return (b.thcAvg ?? -1).compareTo(a.thcAvg ?? -1);
    }
    return BvMonitorAbstract.sortOnDefaultDisplayOrder(a, b);
  }

  static List<BvItem> getGohiProductList(BvMonitor monitor, int tvNumber) {
    // TV1 - Flower
    // TV2 - PreRolls
    // TV3 - Vapes, Concentrates, Edibles, Beverages, Topicals, Oils
    List<BvItem> toReturn = [];
    if (tvNumber == 1) {
      toReturn.addAll(monitor.flowerProductsInStock);
    } else if (tvNumber == 2) {
      toReturn.addAll(monitor.prerollProductsInStock);
    } else if (tvNumber == 3) {
      toReturn.addAll(monitor.vapeProductsInStock);
      toReturn.addAll(monitor.beverageProductsInStock);
      toReturn.addAll(monitor.edibleProductsInStock);
      toReturn.addAll(monitor.oilProductsInStock);
      toReturn.addAll(monitor.topicalProductsInStock);
      toReturn.addAll(monitor.concentrateProductsInStock);
    }

    return toReturn;
  }

  static List<BvItem> getGreenLeafProductList(BvMonitor monitor, int tvNumber) {
    // TV1 - Flower & Prerolls
    // TV2 - Vapes, oils and capsules
    // TV3 - Edibles & Beverages
    List<BvItem> toReturn = [];
    if (tvNumber == 1) {
      toReturn.addAll(monitor.flowerProductsInStock);
      toReturn.addAll(monitor.prerollProductsInStock);
    } else if (tvNumber == 2) {
      toReturn.addAll(monitor.vapeProductsInStock);
      toReturn.addAll(monitor.oilProductsInStock);
      toReturn.addAll(monitor.capsuleProductsInStock);
    } else if (tvNumber == 3) {
      toReturn.addAll(monitor.beverageProductsInStock);
      toReturn.addAll(monitor.edibleProductsInStock);
    }

    return toReturn;
  }

  static List<BvItem> getCanntinaProductList(BvMonitor monitor, int tvNumber) {
    // TV1 - Everything, or at least almost everything
    List<BvItem> toReturn = [];
    if (tvNumber == 1) {
      toReturn.addAll(monitor.flowerProductsInStock);
      toReturn.addAll(monitor.prerollProductsInStock);
      toReturn.addAll(monitor.vapeProductsInStock);
      toReturn.addAll(monitor.oilProductsInStock);
      toReturn.addAll(monitor.capsuleProductsInStock);
      toReturn.addAll(monitor.beverageProductsInStock);
      toReturn.addAll(monitor.edibleProductsInStock);
      toReturn.addAll(monitor.concentrateProductsInStock);
      toReturn.addAll(monitor.topicalProductsInStock);
    }
    return toReturn;
  }

  static List<BvItem> getSmokeLabProductList(BvMonitor monitor, int tvNumber) {
    // TV1 - Flowers & Pre-rolls
    // TV2 - Everything else
    List<BvItem> toReturn = [];
    if (tvNumber == 1) {
      toReturn.addAll(monitor.flowerProductsInStock);
      toReturn.addAll(monitor.prerollProductsInStock);
    } else if (tvNumber == 2) {
      toReturn.addAll(monitor.vapeProductsInStock);
      toReturn.addAll(monitor.oilProductsInStock);
      toReturn.addAll(monitor.capsuleProductsInStock);
      toReturn.addAll(monitor.beverageProductsInStock);
      toReturn.addAll(monitor.edibleProductsInStock);
      toReturn.addAll(monitor.concentrateProductsInStock);
      toReturn.addAll(monitor.topicalProductsInStock);
    }
    return toReturn;
  }

  static List<BvItem> getAlphaProductList(BvMonitor monitor, int tvNumber) {
    // TV1 - Flower
    // TV2 - Prerolls
    // TV3 - Vapes
    // TV4 - Beverages + Edibles
    // TV5 - Concentrates + Oils + Caps
    List<BvItem> toReturn = [];
    if (tvNumber == 1) {
      toReturn.addAll(monitor.flowerProductsInStock);
    } else if (tvNumber == 2) {
      toReturn.addAll(monitor.prerollProductsInStock);
    } else if (tvNumber == 3) {
      toReturn.addAll(monitor.vapeProductsInStock);
    } else if (tvNumber == 4) {
      toReturn.addAll(monitor.beverageProductsInStock);
      toReturn.addAll(monitor.edibleProductsInStock);
    } else if (tvNumber == 5) {
      toReturn.addAll(monitor.concentrateProductsInStock);
      toReturn.addAll(monitor.oilProductsInStock);
      toReturn.addAll(monitor.capsuleProductsInStock);
    }

    return toReturn;
  }

  static List<BvItem> getParadiseProductList(BvMonitor monitor, int tvNumber) {
    // TV1 - Flower Indica
    // TV2 - Flower Sativa Hybrid
    // TV3 - Pre-rolls/Vapes
    // TV4 - Beverages + Edibles + Concentrates + Oils + Caps
    List<BvItem> toReturn = [];
    if (tvNumber == 1) {
      toReturn.addAll(
          monitor.flowerProductsInStock.where((element) => element.isIndica));
    } else if (tvNumber == 2) {
      toReturn.addAll(
          monitor.flowerProductsInStock.where((element) => !element.isIndica));
    } else if (tvNumber == 3) {
      toReturn.addAll(monitor.prerollProductsInStock);
      toReturn.addAll(monitor.vapeProductsInStock);
    } else if (tvNumber == 4) {
      toReturn.addAll(monitor.beverageProductsInStock);
      toReturn.addAll(monitor.edibleProductsInStock);
      toReturn.addAll(monitor.concentrateProductsInStock);
      toReturn.addAll(monitor.oilProductsInStock);
      toReturn.addAll(monitor.capsuleProductsInStock);
    }
    return toReturn;
  }

  //Shared by Cannabis Stop and Taste Buds USA as both use the same layout.
  static List<BvItem> getCannabisStopProductList(
      BvMonitor monitor, int tvNumber) {
    // TV1 - Flower (horizontal)
    // TV2 - Vapes + Prerolls (horizontal)
    // TV3 - Beverages + Edibles + Concentrates + Oils + Caps
    List<BvItem> toReturn = [];
    if (tvNumber == 1) {
      toReturn.addAll(monitor.flowerProductsInStock);
    } else if (tvNumber == 2) {
      toReturn.addAll(monitor.vapeProductsInStock);
      toReturn.addAll(monitor.prerollProductsInStock);
    } else if (tvNumber == 3) {
      toReturn.addAll(monitor.beverageProductsInStock);
      toReturn.addAll(monitor.edibleProductsInStock);
      toReturn.addAll(monitor.concentrateProductsInStock);
      toReturn.addAll(monitor.oilProductsInStock);
      toReturn.addAll(monitor.capsuleProductsInStock);
    }
    return toReturn;
  }

  static List<BvItem> getUptownHerbProductList(
      BvMonitor monitor, int tvNumber) {
    //TV1 - Horizontal, flower and preroll
    //TV2 - Vertical, everything else
    List<BvItem> toReturn = [];
    if (tvNumber == 1) {
      toReturn.addAll(monitor.flowerProductsInStock);
      toReturn.addAll(monitor.prerollProductsInStock);
    } else if (tvNumber == 2) {
      toReturn.addAll(monitor.vapeProductsInStock);
      toReturn.addAll(monitor.concentrateProductsInStock);
      toReturn.addAll(monitor.beverageProductsInStock);
      toReturn.addAll(monitor.edibleProductsInStock);
      toReturn.addAll(monitor.oilProductsInStock);
      toReturn.addAll(monitor.capsuleProductsInStock);
      toReturn.addAll(monitor.topicalProductsInStock);
      toReturn.addAll(monitor.seedProductsInStock);
    }
    return toReturn;
  }

  //=========================================
  // OPTIONAL FUNCTIONS
  //===============================
  static String getFilterByStrainType(BvItem item) {
    //Filter the black image via the code here: https://codepen.io/sosuke/pen/Pjoqqp
    if (item.isSativa)
      return "invert(28%) sepia(77%) saturate(2363%) hue-rotate(345deg) brightness(109%) contrast(91%)"; //red #f44336
    if (item.isIndica)
      return "invert(44%) sepia(76%) saturate(1277%) hue-rotate(184deg) brightness(97%) contrast(97%)"; //blue #2196f3
    if (item.isHybrid)
      return "invert(57%) sepia(12%) saturate(1963%) hue-rotate(73deg) brightness(100%) contrast(91%)"; //green #4caf50
    if (item.isBlend)
      return "invert(57%) sepia(98%) saturate(629%) hue-rotate(320deg) brightness(109%) contrast(101%)"; //orange #ff8c66
    return null;
  }
}
