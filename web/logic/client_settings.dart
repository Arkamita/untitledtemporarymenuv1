import 'package:clickspacetv/api.dart';
import 'package:clickspacetv/api_http.dart';
import 'package:clickspacetv/private.dart';
import 'package:clickspacetv/slide.dart';

import '../display/client_helper.dart';

/* Holds most of the necessary information needed to create a new template menu
* Input the credentials, whether it is Cova or Greenline, the theme setting,
* the client name, the logo (which has to be in the logo folder and the width 
* and height of the logo (if you are using values below the actually size, use 
* the same w/h ratio)).
*/

class ClientSettings {
  //Set the credentials for this client. Only Greenline and Cova supported
  final ClientCredentials credentials;
  //Set the color theme for the menu - dark or light
  final bool useLightTheme;
  //Set the background and the logo file for the menuboard
  final String clientName;
  //Client logo settings - Get the dimension for the width and height
  final String clientLogo;
  //Logo Dimensions
  final int logoWidth;
  final int logoHeight;
  //The function to build the product list
  final List<BvItem> Function(BvMonitor monitor, int tvNumber) getProductList;
  //The client's Jurisdiction, by default it is set to Ontario
  final String jurisdiction;
  //The mode to use for the menu - Should be paged or infinite scroll
  final BvMenuMode mode;

  static const Map<int, ClientSettings> clients = {
    //Out And About Cannabis
    1: ClientSettings(
        credentials: ClientCredentials(
            companyId: "OutAboutCannabis-dot-Budvue",
            locationId: "230306",
            provider: Provider.Cova),
        useLightTheme: true,
        clientName: "OUT & ABOUT",
        clientLogo: "Out&About_Logo_Primary.png",
        logoWidth: 491,
        logoHeight: 434,
        getProductList: ClientHelper.getOutAndAboutProductList),

    //Take Me Home Front (Whatever that means)
    2: ClientSettings(
        credentials: ClientCredentials(
          companyId: 1538,
          locationId: 1539,
        ),
        useLightTheme: true,
        clientName: "TAKE ME HOME",
        clientLogo: "Take Me Home Cannabis.png",
        logoWidth: 892,
        logoHeight: 600,
        getProductList: ClientHelper.getTakeMeHomeProductList),

    //Take Me Home Back - Well, well...
    3: ClientSettings(
        credentials: ClientCredentials(
          companyId: 1538,
          locationId: 1544,
        ),
        useLightTheme: true,
        clientName: "TAKE ME HOME",
        clientLogo: "Take Me Home Cannabis.png",
        logoWidth: 892,
        logoHeight: 600,
        getProductList: ClientHelper.getTakeMeHomeProductList),

    //HoneyPot
    4: ClientSettings(
        credentials: ClientCredentials(
            companyId: "HoneypotCannabisInc-dot--dot-Budvue",
            locationId: "236400",
            provider: Provider.Cova),
        useLightTheme: false,
        clientName: "HONEY POT",
        clientLogo: "HoneyPot logo.jpg",
        logoWidth: 1920,
        logoHeight: 1340,
        getProductList: ClientHelper.getHoneyPotProductList),

    //Rokeby Cannabis
    5: ClientSettings(
        credentials: ClientCredentials(
          companyId: 1816,
          locationId: 1817,
        ),
        useLightTheme: false,
        clientName: "Rokeby Cannabis",
        clientLogo: "Rokeby logo.png",
        logoWidth: 400,
        logoHeight: 122,
        getProductList: ClientHelper.getRokebyProductList),

    //West Coast Cannabis Company - Jasper - 610F Patricia St
    6: ClientSettings(
        credentials: ClientCredentials(
            companyId: "WestCoastCannabisStore-dot-Budvue",
            locationId: "176999",
            provider: Provider.Cova),
        useLightTheme: true,
        clientName: "West Coast Cannabis Store",
        clientLogo: "WCC_.png",
        logoWidth: 1004,
        logoHeight: 700,
        getProductList: ClientHelper.getWCCProductList,
        jurisdiction: BvMonitorSettings.JURISDICTION_AB),

    //Highland Buds
    7: ClientSettings(
        credentials: ClientCredentials(
          companyId: 1907,
          locationId: 1908,
        ),
        useLightTheme: false,
        clientName: "Highland Buds",
        clientLogo: "Highland buds.png",
        logoWidth: 917,
        logoHeight: 754,
        getProductList: ClientHelper.getHighlandProductList,
        jurisdiction: BvMonitorSettings.JURISDICTION_AB,
        mode: BvMenuMode.infiniteScroll),

    //Gohi
    8: ClientSettings(
        credentials: ClientCredentials(
            companyId: "GOHI-dot-Budvue",
            locationId: "243578",
            provider: Provider.Cova),
        useLightTheme: false,
        clientName: "GOHI",
        clientLogo: "gohi.png",
        logoWidth: 830,
        logoHeight: 297,
        getProductList: ClientHelper.getGohiProductList,
        mode: BvMenuMode.infiniteScroll),

    //Green Leaf Cannabis
    9: ClientSettings(
      credentials: ClientCredentials(
          companyId: CstvPasskeys.PROFITEK_KEY,
          locationId: 'GreenLeafCannabisEdmontonAb',
          provider: Provider.Profitek),
      useLightTheme: true,
      clientName: "Green Leaf Cannabis",
      clientLogo: "Green Leaf.png",
      logoWidth: 1040,
      logoHeight: 354,
      jurisdiction: BvMonitorSettings.JURISDICTION_AB,
      getProductList: ClientHelper.getGreenLeafProductList,
    ),

    //Canntina Vancouver
    10: ClientSettings(
      credentials: ClientCredentials(
          companyId: "Canntina-dot-Budvue",
          locationId: "181983",
          provider: Provider.Cova),
      useLightTheme: true,
      clientName: "Canntina",
      clientLogo: "Canntina.png",
      logoWidth: 445,
      logoHeight: 77,
      jurisdiction: BvMonitorSettings.JURISDICTION_BC,
      getProductList: ClientHelper.getCanntinaProductList,
    ),
    //HoneyPot
    11: ClientSettings(
        credentials: ClientCredentials(
            companyId: "AlphaCannabis-dot-Budvue",
            locationId: "240613",
            provider: Provider.Cova),
        useLightTheme: true,
        clientName: "Alpha Cannabis",
        clientLogo: "Alpha logo.png",
        logoWidth: 452,
        logoHeight: 395,
        getProductList: ClientHelper.getAlphaProductList),
    12: ClientSettings(
        credentials: ClientCredentials(
            companyId: 1650, locationId: 1651, provider: Provider.Greenline),
        useLightTheme: true,
        clientName: "Paradise Air",
        clientLogo: "Paradise Air.png",
        logoWidth: 893,
        logoHeight: 664,
        getProductList: ClientHelper.getParadiseProductList),
    13: ClientSettings(
        credentials: ClientCredentials(
            companyId: 2011, locationId: 2012, provider: Provider.Greenline),
        useLightTheme: true,
        clientName: "Cannabis Stop",
        clientLogo: "Cannabis Stop.png",
        logoWidth: 1000,
        logoHeight: 1000,
        getProductList: ClientHelper.getCannabisStopProductList),
    14: ClientSettings(
        credentials: ClientCredentials(
            companyId: "TasteBudsDispensary-dot-Budvue",
            locationId: "215991",
            provider: Provider.Cova),
        useLightTheme: true,
        clientName: "Taste Buds Dispensary",
        clientLogo: "Tastebuds logo.png",
        logoWidth: 1000,
        logoHeight: 1000,
        getProductList: ClientHelper.getCannabisStopProductList,
        jurisdiction: BvMonitorSettings.JURISDICTION_USA),
    15: ClientSettings(
        credentials: ClientCredentials(
            companyId: 1746, locationId: 1957, provider: Provider.Greenline),
        useLightTheme: true,
        clientName: "Smoke Lab Cannabis",
        clientLogo: "Smoke Labs.png",
        logoWidth: 1000,
        logoHeight: 1000,
        getProductList: ClientHelper.getSmokeLabProductList),
    //16: Cannabis Stop - Main St, Shelburne ON (3)
    16: ClientSettings(
        credentials: ClientCredentials(
            companyId: 2011, locationId: 2039, provider: Provider.Greenline),
        useLightTheme: true,
        clientName: "Cannabis Stop",
        clientLogo: "Cannabis Stop.png",
        logoWidth: 1000,
        logoHeight: 1000,
        getProductList: ClientHelper.getCannabisStopProductList),
    17: ClientSettings(
      credentials: ClientCredentials(
        companyId: "UptownHerb-dot-Budvue",
        locationId: "254547",
        provider: Provider.Cova,
      ),
      useLightTheme: true,
      clientName: "Uptown Herb",
      clientLogo: "Uptown-Herb-Logo.png",
      logoWidth: 720,
      logoHeight: 720,
      getProductList: ClientHelper.getUptownHerbProductList,
      mode: BvMenuMode.infiniteScroll,
    ),
  };

  const ClientSettings(
      {this.credentials,
      this.useLightTheme,
      this.clientName,
      this.clientLogo,
      this.logoWidth,
      this.logoHeight,
      this.getProductList,
      this.jurisdiction = BvMonitorSettings.JURISDICTION_ON,
      this.mode = BvMenuMode.paged});
}

enum Provider { Cova, Greenline, Profitek }

class ClientCredentials {
  final dynamic companyId;
  final dynamic locationId;
  final Provider provider;

  const ClientCredentials(
      {this.companyId, this.locationId, this.provider = Provider.Greenline});

  BvCredentials get bvCredentials {
    switch (provider) {
      case Provider.Cova:
        return BvCredentials.iqmetrix(
          CstvPasskeys.IQMETRIX_KEY,
          companyId,
          locationId,
        );
      case Provider.Greenline:
        return BvCredentials.greenline(
          CstvPasskeys.GREENLINE_KEY,
          companyId,
          locationId,
        );
      case Provider.Profitek:
        return BvCredentials.profitek(companyId, locationId);
    }
  }
}
