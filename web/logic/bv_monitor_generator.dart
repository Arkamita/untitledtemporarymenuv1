import 'package:clickspacetv/api_http.dart';
import 'package:clickspacetv/api.dart';
import 'package:clickspacetv/slide.dart';
import '../display/client_helper.dart';
import 'client_settings.dart';

class BvMonitorGenerator {
  static const List<String> _categoryBlocklist = [
    "accessor",
    "vapor",
    "clothing",
    "miscellaneous",
    "apparel",
    "merchandise",
    "shipping",
    "gift",
    "tube",
    "glass",
    "paper",
    "storage",
    "infuser",
    "bong"
  ];
  static const Map<String, String> _nameRemapping = {};
  static const Map<String, String> _nameSplitJoin = {};
  static const Map<String, String> _nameSplitJoinStartOnly = {
    '-': '',
  };
  static const Map<String, String> _nameSplitJoinEndOnly = {
    '-': '',
  };

  static BvMonitor generate(
      int minStock,
      List<String> tvSpecificCategoryBlocklist,
      ClientCredentials clientCredentials) {
    BvCredentials credentials = clientCredentials.bvCredentials;

    BvMonitorSettings monitorSettings = new BvMonitorSettings(
      jurisdiction: ClientSettings.clients[Data.slideSkin]
          .jurisdiction, //set to the most relevant jurisdiction - CANADA only as a last resort
      useProductTests:
          false, //true for Cova clients who are setting THC/CBD by individual packages
      categoryBlocklist:
          _categoryBlocklist, //Monitor will print out all the categories that make it through the blacklist. Add as many as you can!!!
      tvSpecificCategoryBlocklist: tvSpecificCategoryBlocklist,
      //The rest of the settings are mostly for BudvueTouch
    );

    BvItemParseSettings parseSettings = new BvItemParseSettings(
      sizesToMerge: BvItemParseSettings.MERGE_1G_35G_7G_15G_28G,
      preferWeeDb: true,
      preferWeeDbForNames: false,
      preferWeeDbForStrainType:
          false, //overridden by default, try to use the POS for this.
      preferredStrainTypes: [
        BvItem.INDICA,
        BvItem.SATIVA,
        BvItem.HYBRID,
        BvItem.BLEND
      ], //Remove Blend and/or add CBD, if needed
      preferWeeDbForFormat:
          true, //Especially with Cannabis 2.0, this should almost always be true
      useWeeDbForFallbackThcCbd:
          true, //If the client *never* wants THC/CBD from the province, set this to false.
      minimumStock: minStock ?? 1, //set via text1, typically
      nameRemapping:
          _nameRemapping, //For 1:1 overwrites of specific product names. Case sensitive
      nameSplitJoin:
          _nameSplitJoin, //For replacing redundant strings (like "PreRoll"). Not case sensitive
      nameSplitJoinStartOnly:
          _nameSplitJoinStartOnly, //For replacing redundant strings at the start of names (like "Flower - "). Not case sensitive
      nameSplitJoinEndOnly:
          _nameSplitJoinEndOnly, //For replacing redundant strings at the end of names (like " - 1g"). Not case sensitive
    );

    BvMonitor monitor = new BvMonitor(
      credentials: credentials,
      monitorSettings: monitorSettings,
      parseSettings: parseSettings,
    );

    return monitor;
  }
}
