# TemplateMenuV1

* **Single/Multi Location:** Multiple clients
* **City, Province(s):** Any
* **POS:** Cova/Greenline or Profitek
* **Orientation:** Vertical or Horizontal (Specified by app orientation data)
* **Total TVs:** Variable
* **Infinite/Paged/Static/Select Menu:** Infinite, Paged, Static or Select

### Summary / Notes / Description
Temporary placeholder menu for clients who wish to go live before their menus are complete. 

### TV Layouts
```
Client specific. Check client_helper for the listing.
```

### Skins (Total Tvs)
```
1 : Out and About Cannabis (3)
2 : Take Me Home Front (3)
3 : Take Me Home Back (3)
4: Honey Pot Smoke House (3)
5: Rokeby Cannabis (3)
6: West Coast Cannabis Company - Jasper - 610F Patricia St (4)
7: Highland Buds - AB (7)
8: Gohi (3)
9: Greem Leaf Cannabis - AB (3)
10: Canntina - BC (1)
11: Alpha Cannabis - ON (5)
12: Paradise Air - ON (4)
13: Cannabis Stop - George St, Arthur, ON (3)
14: Taste Buds (USA) (3)
15: Smoke Labs - ON (2)
16: Cannabis Stop - Main St, Shelburne ON (3)
17: Uptown Herb - 56 King St N Waterloo, ON (2)
```

### text1-6 settings
 * text1 = Minimum Stock to Display (Default 1)
 * text2 = Scrolling speed in px/s (Default 40)
 * text2 = Seconds between screen transitions (Default 15)
 * text3 = TV Number (1-3)
 * text4 = Total Number of TVs (Default 3)
 

### Debug Testing
To test the slide, two terminal instances required, both from local directory
```
python3 -m http.server 8081        #To serve video content
webdev serve                       #To serve slide
```

#### Deployment
To package & name for deployment:
```
webdev build && buildpath=${PWD##*/} && cd build && zip -r ../_${buildpath}_$(date -u +%Y%m%d_%H%M).zip slide.dart.js slide.css slide.html assets/ && cd ..
```